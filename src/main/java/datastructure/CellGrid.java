package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {

    //lagt in feltvariablene
    int cols;
    int rows;
    //grid er en array av en array. todimensjonalt. fylles med typen/klassen Cellstate som er en enum. bruk av denne er generics
    CellState[][] grid;



    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        //konstruktører for feltvariablene
        this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];






    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;  //returnerer antall rader. rader er definert i feltvariabelen.
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element){
        // TODO Auto-generated method stub
        if(row >= this.numRows() || column>=this.numColumns()) {
            throw new IndexOutOfBoundsException("too high index");
        }
        if(row < 0 || column<0) {
            throw new IndexOutOfBoundsException("Too low index");
        }
        grid[row][column] = element; //legger til element på en gitt rad/kolonne
    }




    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row >= this.numRows() || column>=this.numColumns()) {
            throw new IndexOutOfBoundsException("too high index");
        }
        if(row < 0 || column<0) {
            throw new IndexOutOfBoundsException("Too low index");
        }
        return grid[row][column];

    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for(int row=0; row<this.rows; row++) {
            for(int col=0; col<this.cols;col++) {
                CellState value = this.get(row, col);
                newGrid.set(row, col, value);
            }
        }
        return newGrid;

    }
    
}
